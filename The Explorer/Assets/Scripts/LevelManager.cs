﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    [System.Serializable]
    public class Level
    {
        public string LevelText;
        public int Unlocked;
        public bool IsInteracteble;
    }

    public GameObject levelButton;
    public Transform Spacer;

    public List<Level> LevelList;
    // Start is called before the first frame update
    void Start()
    {
        //DeleteAll();
        FillList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FillList()
    {
        foreach(var level in LevelList)
        {
            //creaza clone
            GameObject newButton = Instantiate(levelButton) as GameObject;

            LevelButtonManager levelButtonManager = newButton.GetComponent<LevelButtonManager>();
            levelButtonManager.LevelText.text = level.LevelText;

            if(PlayerPrefs.GetInt("Level"+levelButtonManager.LevelText.text) == 1)
            {
                level.Unlocked = 1;
                level.IsInteracteble = true;
            }

            levelButtonManager.Unlocked = level.Unlocked;
            levelButtonManager.GetComponent<Button>().interactable = level.IsInteracteble;
            levelButtonManager.GetComponent<Button>().onClick.AddListener(() => loadLevels("Level"+ levelButtonManager.LevelText.text));

            //seteaza parintele unde  sa se creeze clonele
            newButton.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);
            newButton.transform.SetParent(Spacer);
        }

        SaveAll();
    }

    void SaveAll()
    {

        if(PlayerPrefs.HasKey("Level1"))
        {
            return;
        }
        else
        {
            GameObject[] allButtons = GameObject.FindGameObjectsWithTag("LevelButton");
            foreach (GameObject button in allButtons)
            {
                LevelButtonManager levelButtonManager = button.GetComponent<LevelButtonManager>();
                PlayerPrefs.SetInt("Level"+levelButtonManager.LevelText.text, levelButtonManager.Unlocked);
            }
        }
      
    }

    void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }


    public void ToMenu()
    {
        SceneManager.LoadScene("Menu");

    }

    void loadLevels(string value)
    {
        Application.LoadLevel(value);
    }
}
