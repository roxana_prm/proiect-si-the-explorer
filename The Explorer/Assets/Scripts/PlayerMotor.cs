﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 moveVector;

    private float speed = 5.0f;
    private float verticalVelocity = 0.0f;
    private float gravity = 12.0f;

    private float animationDuration = 2.0f;
    private float startTime;


    private bool isDead = false;

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        //controller.enabled = true;
        startTime = Time.time;

    }

    // Update is called once per frame
    void Update()
    {

        if (isDead)
        {
            return;
        }

       if (transform.position.z > 400)
            {
                Debug.Log("Finish");
                FinishLevel();
                GetComponent<Score>().OnFinish();

        }



        if (Time.time - startTime < animationDuration)
        {
            controller.Move(Vector3.forward * speed * Time.deltaTime);
            //iese din update function and no execute the rest of the code
            return;
        }
        

        moveVector = Vector3.zero;

        //daca caracterul este pe plan
        if(controller.isGrounded)
        {
            verticalVelocity = -0.5f;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        //X -> left and right
        moveVector.x = Input.GetAxisRaw("Horizontal") * speed;

        //Y -> up and down 
        //moveVector.y = verticalVelocity;

        //Z -> forward and backward
        moveVector.z = speed;

        controller.Move(moveVector * Time.deltaTime);
        //controller.Move((Vector3.forward * speed) * Time.deltaTime);   
    }

    public void SetSpeed(float modifier)
    {
        speed = 5.0f + modifier;
    }

    //it is being called every time our capsule hits something
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        //point is impact point
        if (hit.transform.tag == "coin")
        {
            print("coin");
            Debug.Log("cooooin");
            Destroy(hit.gameObject);
            GetComponent<Score>().CollectCoin();

        }
        else
        {
            if (hit.point.z > transform.position.z + controller.radius)
            {
                Death();
            }
        }
        
        //if impact with a coin
       
    }

    private void Death()
    {
        isDead = true;
        GetComponent<Score>().OnDeath();
        //Debug.Log("Deatth");
    }

    private void FinishLevel()
    {
        
        PlayerPrefs.SetInt("Level2", 1);
        Application.LoadLevel(2);//indexul din build de la lelev select
        return;
    }

  
}
