﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
public class RoadManager : MonoBehaviour
{
    public GameObject[] roadPrefabs;

    private Transform playerTransform;
    private float spawnz = -10.0f; /// where in z put the object
    private float roadLength = 20.0f;
    private int amountRoadOnScreen = 7;

    private List<GameObject> activeRoads;
    private float safeZone = 40.0f;

    private int lastPrefabIndex = 0;

    // Start is called before the first frame update
    private void Start()
    {
        activeRoads = new List<GameObject>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

        for (int i=0; i < amountRoadOnScreen; i++)
        {
            if (i < 2)
                SpawnRoad(0);
            else
                SpawnRoad();
        }
        
    }

    // Update is called once per frame
    private void Update()
    {
        if(playerTransform.position.z -safeZone > (spawnz - amountRoadOnScreen * roadLength))
        {
            SpawnRoad();
            DeleteRoad();

        }
    }

    private void SpawnRoad(int prefabIndex = -1)
    {
        GameObject go;

        if (prefabIndex == -1)
            go = Instantiate(roadPrefabs[RandomPrefabIndex()]) as GameObject;
        else
            go = Instantiate(roadPrefabs[prefabIndex]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnz;
        spawnz += roadLength;
        activeRoads.Add(go);
    }

    private void DeleteRoad()
    {
        Destroy(activeRoads[0]);
        activeRoads.RemoveAt(0);
    }

    private int RandomPrefabIndex()
    {
        if (roadPrefabs.Length <= 1)
            return 0;
        int randomIndex = lastPrefabIndex;
        while(randomIndex == lastPrefabIndex)
        {
            randomIndex = Random.Range(0, roadPrefabs.Length);
        }

        lastPrefabIndex = randomIndex;
        return randomIndex;
    }
    
}

