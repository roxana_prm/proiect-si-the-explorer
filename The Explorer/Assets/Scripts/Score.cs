﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//give access to the text component
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private float score = 0.0f;

    //this below element is equal with scoreText Ui
    public Text scoreText;

    private int difficultyLevel = 1;
    private int maxDifficultyLevel = 10;
    private int scoreToNextLevel = 100;

    private bool isDeath = false;

    public DeathMenu deathMenu;

    // Start is called before the first frame update
    void Start()
    {
       

    }

    // Update is called once per frame
    void Update()
    {
        if (isDeath)
            return;

        if (score >= scoreToNextLevel)
            LevelUp();

        //score += Time.deltaTime + difficultyLevel;
        scoreText.text = "Score:" + ((int)score).ToString();
    }

    void LevelUp()
    {
        if (difficultyLevel == maxDifficultyLevel)
            return;
        //scoreToNextLevel *= 2;
        scoreToNextLevel += scoreToNextLevel;
        difficultyLevel++;

        GetComponent<PlayerMotor>().SetSpeed(difficultyLevel);
        //Debug.Log(difficultyLevel);
    }

    public void CollectCoin()
    {
        score += 10;
    }

    public void CollectDiamond()
    {
        score += 100;
    }

    public void OnDeath()
    {
        isDeath = true;
        
        deathMenu.ToggleEndMenu(score);
    } 

    public void OnFinish()
    {
        if (PlayerPrefs.GetFloat("Highscore") < score)
            PlayerPrefs.SetFloat("Highscore", score);
    }
}
